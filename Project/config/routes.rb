Rails.application.routes.draw do
  devise_for :users
  root 'static_pages#home'
  resources :users, only: [:index, :show]
  resources :posts do
    resources :comments
  end
  resources :relationships, only: [:create, :destroy, :index]
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
